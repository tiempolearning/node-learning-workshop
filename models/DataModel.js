(() => {
    'use strict';

    const mongoose = require('mongoose');

    const Schema = mongoose.Schema;

    const DataModel = new Schema({
        name: String,
        age: Number,
        registration_date: Date
    });

    module.exports = mongoose.model('DataModel', DataModel)
})();