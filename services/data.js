(() => {
    'use strict';

    const dataModel = require('../models/DataModel');

    /**
     * Data service
     */

    const createData = (data, callback) => {
        const newData = new dataModel(data);

        newData.save((error, dataSaved) => {
            if (!error) {
                return callback({
                    success: true,
                    data: dataSaved
                });
            } else {
                return callback({
                    success: false,
                    error
                });
            }
        });
    };

    const getSingleData = () => { };

    const getAllData = () => { };

    const updateData = () => { };

    const deleteData = () => { };

    module.exports = {
        createData,
        getSingleData,
        getAllData,
        updateData,
        deleteData
    };
})();