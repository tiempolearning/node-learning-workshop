(() => {
    'use strict';

    const express = require('express');
    const mongoose = require('mongoose');
    const bodyParser = require('body-parser');

    module.exports = (sourcePath, app) => {
        const rootPath = sourcePath;
        const baseApp = app;
        var mainConnection = null;

        const configFile = require(`${rootPath}/config/local.json`);

        const getPort = () => configFile.port;

        const getAssetsRoute = () => `${rootPath}/site/assets`;

        const getIndexRoute = () => `${rootPath}/site/index.html`;

        const getMongoPath = () => configFile.mongo_path;

        const configRoutes = () => {
            baseApp.use('/assets', express.static(getAssetsRoute()));

            baseApp.get('/', (req, res, next) => {
                res.sendFile(getIndexRoute())
            });

            app.use(bodyParser.urlencoded({
                extended: true,
                limit: configFile.body_parser_limit
            }));

            app.use(bodyParser.json({
                limit: configFile.body_parser_limit
            }))

            require('../routes/app')(baseApp);
        };

        const configureConnection = () => {
            mainConnection = mongoose.connect(getMongoPath(), {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false
            });
        };

        const init = () => {
            configRoutes();
            configureConnection();
        };

        return {
            getPort,
            getAssetsRoute,
            getIndexRoute,
            getMongoPath,
            init
        }
    };
})();