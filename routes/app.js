(() => {
    'use strcit';

    const routes = require('../config/routes');

    module.exports = app => {
        app.get(routes.API_GET_SINGLE_DATA,
            (req, res, next) => {
                return res.status(200).json({
                    success: true,
                    message: 'This is a message',
                    data: {
                        something: 'This is the data',
                        number: 1,
                        date: new Date(),
                        string: 'This is a string'
                    }
                });
            }
        );

        app.post(routes.API_INSERT_DATA,
            (req, res, next) => {
                const dataBody = req.body;

                return res.status(201).json({
                    success: true,
                    message: 'Data creation success',
                    data: dataBody
                });
            }
        );
    };
})();