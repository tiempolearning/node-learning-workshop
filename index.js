(() => {
    'use strict';

    const express = require('express');
    const app = express();
    const server = require('http').createServer(app);

    const config = require('./lib/config')(__dirname, app);

    const port = config.getPort();

    config.init();

    server.listen(port);

    console.log('Server listening on port', port);
})();