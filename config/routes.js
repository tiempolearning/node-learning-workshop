(() => {
    'use strict';

    module.exports = {
        API_INSERT_DATA: '/data',

        API_GET_ALL_DATA: '/datas',

        API_GET_SINGLE_DATA: '/data',

        API_UPDATE_DATA: '/data/:id',

        API_DELEETE_DATA: '/data/:id'
    };
})();